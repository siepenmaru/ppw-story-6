from django.shortcuts import render
from .models import OpinionBoard
from .forms import OpinionBoardForm

# Create your views here.
def index(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = OpinionBoardForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            opinions = OpinionBoard.objects.all()
            form = OpinionBoardForm()
            # redirect to a new URL:
            return render(request, "index.html", {"opinions":opinions, 'form': form})

    # if a GET (or any other method) we'll create a blank form
    form = OpinionBoardForm()
    opinions = OpinionBoard.objects.all()
    
    return render(request, 'index.html', {"opinions":opinions, 'form': form})