from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class OpinionBoard(models.Model):
    author = models.CharField(max_length=30)
    msg = models.CharField(max_length=50)
    date_time_posted = models.DateTimeField(auto_now_add= True)
    date_time_updated = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.msg